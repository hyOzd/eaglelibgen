#include "eaglelibgen.h"
#include "eaglecomgen.h"

using namespace std;


string eaglelibgen::draw_wire_rect(float x, float y, float width, float height) {

	stringstream cmdss;

	cmdss<<eaglecomgen::draw_wire(x,y,x+width,y)<<endl;
	cmdss<<eaglecomgen::draw_wire(x+width,y,x+width,y+height)<<endl;
	cmdss<<eaglecomgen::draw_wire(x+width,y+height,x,y+height)<<endl;
	cmdss<<eaglecomgen::draw_wire(x,y+height,x,y)<<endl;

	return cmdss.str();

}

string eaglelibgen::draw_symbol(DeviceDescription::SymbolStyle style, vector<string> pin_name_list) {

	stringstream cmdss;

	//first find longest string in pin_name_list, will be used to determine width/height of drawing
	int lsmax = 0;
	int n = pin_name_list.size();
	for(int i=0; i<pin_name_list.size(); i++){
		if(pin_name_list.at(i).length()>lsmax) lsmax = pin_name_list.at(i).length();
	}

	float width,height;

	cmdss<<eaglecomgen::change_layer(eaglecomgen::L_SYMBOLS)<<endl;

	if(style!=DeviceDescription::SS_QUAD){

		//calculate width and height
		if(style==DeviceDescription::SS_DUAL){
			height = grid_size*((n%2 ? n+1:n)/2+1);
			width = grid_size*(lsmax+5);
		}else{
			height = grid_size*(n+1);
			width = grid_size*(lsmax/2+3);
		}

		//place pins
		if(style==DeviceDescription::SS_LEFT){
			cmdss<<eaglelibgen::draw_wire_rect(2*grid_size,0,width,height);
			for(int i=0; i<pin_name_list.size(); i++){
				cmdss<<eaglecomgen::add_pin(pin_name_list[i],0,height-grid_size*(1+i))<<endl;
			}
		}else if(style==DeviceDescription::SS_RIGHT){
			cmdss<<eaglelibgen::draw_wire_rect(0,0,width,height);
			for(int i=0; i<pin_name_list.size(); i++){
				cmdss<<eaglecomgen::add_pin(pin_name_list[i],width+2*grid_size,height-grid_size*(1+i),180)<<endl;
			}
		}else{
			cmdss<<eaglelibgen::draw_wire_rect(2*grid_size,0,width,height);
			int rl = n%2 ? n+1:n;
			for(int i=0; i<rl/2; i++){
				cmdss<<eaglecomgen::add_pin(pin_name_list[i],0,height-grid_size*(1+i))<<endl;
			}
			for(int i=rl/2; i<n; i++){
				cmdss<<eaglecomgen::add_pin(pin_name_list[i],width+4*grid_size,grid_size*(i-rl/2+1),180)<<endl;
			}
		}

		//place texts
		cmdss<<eaglecomgen::change_layer(eaglecomgen::L_NAMES)<<endl;
		cmdss<<eaglecomgen::add_text(">NAME",0,height+grid_size)<<endl;
		cmdss<<eaglecomgen::change_layer(eaglecomgen::L_VALUES)<<endl;
		cmdss<<eaglecomgen::add_text(">VALUE",0,-2*grid_size)<<endl;

	}else{

		int rl = n;
		if(n%4>0) rl =  n+(4-n%4);
		int ns = rl/4;	//number of pins in one side

		width = grid_size*(ns+lsmax+4);
		height = grid_size*(ns+lsmax+4);

		cmdss<<eaglelibgen::draw_wire_rect(2*grid_size,2*grid_size,width,height)<<endl;

		//place top side pins
		for(int i=0; i<ns; i++){
			if(i>=pin_name_list.size()) break;	//WARNING:easy solution :-(
			cmdss<<eaglecomgen::add_pin(pin_name_list[i],grid_size*(6+i),grid_size*4+height,270)<<endl;
		}
		//place right side pins
		for(int i=ns; i<2*ns; i++){
			if(i>=pin_name_list.size()) break;
			cmdss<<eaglecomgen::add_pin(pin_name_list[i],4*grid_size+width,height+grid_size*(-2-i%ns),180)<<endl;
		}
		//place bottom side pins
		for(int i=2*ns; i<3*ns; i++){
			if(i>=pin_name_list.size()) break;
			cmdss<<eaglecomgen::add_pin(pin_name_list[i],width+grid_size*(-2-i%ns),0,90)<<endl;
		}
		//place left side pins
		for(int i=3*ns; i<4*ns; i++){
			if(i>=pin_name_list.size()) break;
			cmdss<<eaglecomgen::add_pin(pin_name_list[i],0,grid_size*(6+i%ns),0)<<endl;
		}

		//place texts
		cmdss<<eaglecomgen::change_layer(eaglecomgen::L_NAMES)<<endl;
		cmdss<<eaglecomgen::add_text(">NAME",0,height+grid_size*5)<<endl;
		cmdss<<eaglecomgen::change_layer(eaglecomgen::L_VALUES)<<endl;
		cmdss<<eaglecomgen::add_text(">VALUE",0,-2*grid_size)<<endl;

	}

	return cmdss.str();

}


string eaglelibgen::generate_symbol(DeviceDescription::Gate gate,string prefix,bool multi_pin){

	stringstream cmdss;

	//generate commands to create symbol
	cmdss<<eaglecomgen::edit_symbol(prefix+gate.name)<<endl;

	//create pin name list
	vector<string> pin_name_list;
	if (multi_pin) {

		for(int i=0; i<gate.pin_list.size(); i++){
			pin_name_list.push_back(gate.pin_list[i].uname);
		}

	} else {

		for(int i=0; i<gate.pin_list.size(); i++){

			if(gate.pin_list[i].name==gate.pin_list[i].uname){

				pin_name_list.push_back(gate.pin_list[i].name);

			}

		}

	}

	//generate commands to draw symbol
	cmdss<<draw_symbol(gate.style,pin_name_list);

	return cmdss.str();

}

string eaglelibgen::generate_device(DeviceDescription device, generate_options options){

    stringstream cmdss;

	//gen.com. to create symbols
	for(int i=0; i<device.gate_list.size(); i++){
		cmdss<<generate_symbol(device.gate_list[i],device.name+"-",options.multi_pin);
	}

	//generate commands to create device
	cmdss<<eaglecomgen::edit_device(device.name)<<endl;

	//gc to add gates to device
	stringstream ggname;
	for(int i=0; i<device.gate_list.size(); i++){
		cmdss<<eaglecomgen::add_symbol(device.name+"-"+device.gate_list[i].name,device.gate_list[i].name,0,i*grid_size*10)<<endl;
	}

	//add package
	if(device.package_library.empty()){
		cmdss<<eaglecomgen::add_package(device.package_name,"")<<endl;
	}else{
		cmdss<<eaglecomgen::add_package(device.package_name,device.package_library,"")<<endl;
	}

	cmdss<<generate_connects(device,options.multi_pin);

	return cmdss.str();

}

string eaglelibgen::generate_device(DeviceDescription device){

    generate_options opt;
    opt.multi_pin = true;

	return generate_device(device,opt);

}

string eaglelibgen::generate_connects(DeviceDescription device,bool multi_pin){

	stringstream cmdss;

	if(multi_pin){

		for(int i=0; i<device.gate_list.size(); i++){
			for(int j=0; j<device.gate_list[i].pin_list.size(); j++){
				//single pin to single pad connect
				cmdss<<eaglecomgen::connect(eaglecomgen::ALL,device.gate_list[i].name,device.gate_list[i].pin_list[j].uname,device.gate_list[i].pin_list[j].pad)<<endl;
			}
		}

	} else {

		for(int i=0; i<device.gate_list.size(); i++) {

			for(int j=0; j<device.gate_list[i].pin_list.size(); j++){

				//check if this is a copy or not
				if(device.gate_list[i].pin_list[j].name==device.gate_list[i].pin_list[j].uname){

					vector<string> pad_name_list;
					pad_name_list.push_back(device.gate_list[i].pin_list[j].pad);

					//find copies of this
					//assumes copies comes after :S
					for(int k=j+1; k<device.gate_list[i].pin_list.size(); k++){

						if(device.gate_list[i].pin_list[j].name==device.gate_list[i].pin_list[k].name){

							pad_name_list.push_back(device.gate_list[i].pin_list[k].pad);

						}

					}

					//generate connects
					if(pad_name_list.size()<2){
						//single pin to single pad connect
						cmdss<<eaglecomgen::connect(eaglecomgen::ALL,device.gate_list[i].name,device.gate_list[i].pin_list[j].name,device.gate_list[i].pin_list[j].pad)<<endl;
					} else {
						//single pin to multiple pad connect
						cmdss<<eaglecomgen::connect(device.gate_list[i].name,device.gate_list[i].pin_list[j].name,pad_name_list,eaglecomgen::ALL)<<endl;
					}

				}

			}

		}

	}

	return cmdss.str();

}

