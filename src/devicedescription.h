#ifndef DEVICEDESCRIPTION_H
#define DEVICEDESCRIPTION_H

#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>

#include "devicedescription.h"

/** \brief Contains information and functions to create a complete device
 *
 *	DeviceDescription class has a hiyerarcy as fallows \n
 *	\n
 *	Device\n
 *		->gates \n
 *			->pins(pad)\n
 *
 */
class DeviceDescription {

	public:

		DeviceDescription();

		enum SymbolStyle{SS_LEFT,SS_RIGHT,SS_DUAL,SS_QUAD};

		struct Pin{
			std::string name;
			std::string uname;///< unique name;actual name of pin in symbols, this is important in case there are multiple pins with the same name
			std::string pad;
		};

		struct Gate{
			std::string name;
			std::string rname;///< Name of the gate when added to device
			SymbolStyle style;
			std::vector<Pin> pin_list;
		};

		friend class DevDscFileParser;

		std::string name;				///< Name of the device
		std::string package_name;		///< Name of package, currently package generation not supported!
		std::string package_library;	///< Name of library packace exists

		std::vector<Gate> gate_list;

        /** \brief Adds a gate to device, each device should have at least one gate
         *
         * \param name \e std::string Name of the gate
         * \param style \e SymbolStyle Symbol style of gate in schematic
         * \return \e bool false if gate exists already
         *
         */
		bool add_gate(std::string name,SymbolStyle style);

        /** \brief Adds a new pin to device
         *
         * \param name \e std::string	Name of the pin
         * \param gate_name \e std::string Name of the gate which pin will be added
         * \param pad \e std::string	Name of the pad which pin connects in package
         * \return \e bool false if gate name deosn't added already
         *
         */
		bool add_pin(std::string name,std::string gate_name,std::string pad);


        /** \brief Sets the package of device
         *
         * \param name \e std::string	Name of the package
         * \param library_name \e std::string	Library which contains the package given
         * \return \e void
         *
         */
		void set_package(std::string name, std::string library_name);

        /** \brief Reads description from a file
         *
         * \param file_name \e std::string
         * \return \e bool
         *
         */
		bool read_from_file(std::string file_name);

		std::string toString();
		std::string symstyle_toString(SymbolStyle ss);

	private:

        /** \brief Parses string input, probably from a file
         *
         * \param str_input \e std::string
         * \return \e bool
         *
         */
		bool parse_input(std::string str_input);
		void create_unames();

};

#endif // DEVICEDESCRIPTION_H
