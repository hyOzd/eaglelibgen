#ifndef DEVDSCFILEPARSER_H
#define DEVDSCFILEPARSER_H

#include <vector>
#include <list>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <boost/regex.hpp>
#include "devicedescription.h"

class DevDscFileParser
{
	public:
		DevDscFileParser();
		virtual ~DevDscFileParser();

        /** \brief Will parse given input file
         *	If success returns true. In failure returns false
         *	and prints some errors to std::cout . After success
         *	use get_devdsc to get DeviceDescription which
         *	contains device information.
         *
         * \param file_name std::string Name of the device description file
         * \return bool
         *
         */
		bool parse_file(std::string file_name);

        /** \brief Returns DeviceDescription which carries information
         *	Use this function to get DeviceDescription after
         *	success of parse_file()
         *
         * \return DeviceDescription
         *
         */
		DeviceDescription get_devdsc();

		void print_lines(); ///< Used for Debug purposes


	private:

		enum Commands{CDEVICE=1,CPACKAGE,CGATES,CPINS};

		DeviceDescription devdsc;	// our device description to read into

		std::list<std::string> lines;
		DeviceDescription::SymbolStyle def_sym_style;	//read from >DEVICE command
		bool gates_given,pads_given;
		unsigned int pin_index;

		bool parse_lines();
		void make_cleaning();
		DevDscFileParser::Commands is_command(const std::string& str);

		// read data functions
		bool read_device_command(const std::string& line);	//this function reads device name from the line and if it's specified symbol type
		bool read_symbol_style(const std::string& str, DeviceDescription::SymbolStyle& ss);
		bool read_package_command(const std::string& line);
		bool read_gates_command(const std::string& line);
		bool read_pins_command(const std::string& line);

		std::vector<std::string> parse_padnames(std::string spadnames);	/**< Parses comma separated pad names like: 1,2,3 */
};

#endif // DEVDSCFILEPARSER_H
