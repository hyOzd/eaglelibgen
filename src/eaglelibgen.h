#ifndef		_EAGLELIBGEN_
#define		_EAGLELIBGEN_

#include <string>
#include <vector>
#include <sstream>
#include "devicedescription.h"

/** \brief This is a library of functions to generate
 *	commands to create symbols,packages and devices
 *	in Eagle PCB Layout software. This library uses
 *	functions defined in eaglecomgen namespace.
 *
 */
namespace eaglelibgen{

	using namespace std;

	const float grid_size = 0.1;

	struct generate_options{
        bool multi_pin;
	};

    /** \brief Generates commands to draw a rectangle with wires
     *	in current layer
     * \param x \e float
     * \param y \e float
     * \param width \e float
     * \param height \e float
     * \return \e string Eagle commands string
     *
     */
	string draw_wire_rect(float x, float y, float width, float height);

    /** \brief Generates commands to draw a symbol with given list of pin names
     *
     *	This functions creates symbol rectangle, places pins,
     *	places name and value texts. \n
     *	\n
     *	For RSS_QUAD style pin_name_list must have at least 4
     *	elements. But having a number of 4*X elements is advised.
     *	If it's not a factor of 4, behavior is unknown.
     * \param style \e DeviceDescription::SymbolStyle			Style of symbol
     * \param pin_name_list \e vector<string>	list of pin names
     * \return \e string Eagle commands string
     *
     */
	string draw_symbol(DeviceDescription::SymbolStyle style, vector<string> pin_name_list);

	string generate_device(DeviceDescription device);
	string generate_device(DeviceDescription device, generate_options options);

    /** \brief Generates commands to create a new symbol for given
     *
     * If multi_pin is false, first a list of unique named pins
     * will be generated. For this, function makes a comparison
     * of Pin.name and Pin.uname. If they are not the same
     * this pin won't be included in list.
     *
     * \param gate \e DeviceDescription::Gate
     * \param prefix="" \e string
     * \param single_pin=false \e bool if a pin will be generated for each multi pad pin (like GND pins)
     * \return \e string
     *
     */
	string generate_symbol(DeviceDescription::Gate gate,string prefix="",bool multi_pin=true);

	string generate_connects(DeviceDescription device, bool multi_pin=true);

}

#endif
