#ifndef _EAGLECOMGEN_
#define _EAGLECOMGEN_

#include <string>
#include <sstream>
#include <vector>


/** \brief The library of functions which generates Eagle commands
 *	This library generates commands for Eagle which then
 *	can be written to a .scr file and run inside Eagle
 *	to automatically generate library symbols, packages
 *	and devices.
 *
 *	These functions only generate one single command. Strings doesn't
 *	have newline at the end. Remember to add them when generating script
 *	files.
 */
namespace eaglecomgen{

	using namespace std;

	enum Layer{	L_TOP 			= 1,
				L_BOTTOM 		= 16,
				L_PADS			= 17,
				L_VIAS			= 18,
				L_UNROUTED		= 19,
				L_DIMENSION		= 20,
				L_TPLACE		= 21,
				L_BPLACE		= 22,
				L_TORIGINS		= 23,
				L_BORIGINS		= 24,
				L_TNAMES		= 25,
				L_BNAMES		= 26,
				L_TVALUES		= 27,
				L_BVALUES		= 28,
				L_TSTOP			= 29,
				L_BSTOP			= 30,
				L_TCREAM		= 31,
				L_BCREAM		= 32,
				L_TKEEPOUT		= 39,
				L_BKEEPOUT		= 40,
				L_DRILLS		= 41,
				L_HOLES			= 42,
				L_NETS			= 91,
				L_BUSSES		= 92,
				L_PINS			= 93,
				L_SYMBOLS		= 94,
				L_NAMES			= 95,
				L_VALUES		= 96,
				L_INFO			= 97,
				L_GUIDE			= 98
				};

	enum ConnectType{NONE,ALL,ANY};

	enum PadShape{DEFAULT,SQUARE,ROUND,OCTAGON,LONG,OFFSET};

	enum PinOptions{	PIN_DEFAULT = 0x00000000,
						PIN_NC 		= 0x00000001,
						PIN_IN 		= 0x00000002,
						PIN_OUT 	= 0x00000004,
						PIN_IO		= 0x00000008,
						PIN_OC		= 0x00000010,
						PIN_HZ		= 0x00000020,
						PIN_PAS		= 0x00000040,
						PIN_PWR		= 0x00000080,
						PIN_SUP		= 0x00000100,
						PIN_DOT		= 0x00000200,
						PIN_CLK		= 0x00000400,
						PIN_DOTCLK	= 0x00000800,
						PIN_POINT	= 0x00001000,
						PIN_SHORT	= 0x00002000,
						PIN_MIDDLE	= 0x00004000,
						PIN_LONG	= 0x00008000,
						PIN_VOFF	= 0x00010000,
						PIN_VPAD	= 0x00020000,
						PIN_VPIN	= 0x00040000,
						PIN_VBOTH	= 0x00080000
						};

    /** \brief Generates command to open a library file for editing
     *
     * \param library_name \e string Name of the library file with extension. e.g. "nameoflibrary.lbr"
     * \return string Eagle command string
     *
     */
	string open_library(string library_name);

    /** \brief Generates command to copy a package from another library
     *
     * \param package_name \e string Name of the package to be copied
     * \param from_library_name \e string Name of the library which contains package
     * \return \e string Eagle command string
     *
     */
	string copy_package(string package_name,string from_library_name);

    /** \brief Generates command to adds symbol to the device
     *	Use this if device has more than one symbol
     * \param symbol_name \e string name of the symbol in library
     * \param gate_name \e string  gate name of the symbol in this device, such as "A1, A2" etc.
     * \param x \e float position x
     * \param y \e float position y
     * \return \e string Eagle command string
     *
     */
	string add_symbol(string symbol_name, string gate_name, float x, float y);

	/** \brief Generates command to adds symbol to the device
     *	Use this if device has only one symbol in schematic
     * \param symbol_name \e string name of the symbol in library
     * \param x \e float position x
     * \param y \e float position y
     * \return \e string Eagle command string
     *
     */
	string add_symbol(string symbol_name, float x, float y);

    /** \brief Generate 'edit' command
	 *
     *	Edit command opens 'name' to edit or creates
     *	a new one if 'name' doesn't exist. 'name' can be
     *	name of a drawing, symbol, package etc. E.g. \n
     *	\n
	 *	name.sym	loads symbol \n
	 *	name.pac	loads package \n
	 *	name.dev	loads device \n
	 *	name.brd	loads board \n
	 *	name.sch	loads schematic \n
     * \param name \e string name of the drawing to edit
     * \return \e string Eagle command string
     *
     */
	string edit(string name);

    /** \brief Generates command to edit/create a symbol
     *
     * \param symbol_name \e string name of the symbol to edit/create without ".sym" extension
     * \return \e string Eagle command string
     *
     */
	string edit_symbol(string symbol_name);

    /** \brief Generates command to edit/create a package
     *
     * \param package_name \e string name of the package to edit/create
     * \return \e string Eagle command string
     *
     */
	string edit_package(string package_name);

    /** \brief Generate command to edit/create a device
     *
     * \param device_name \e string name of the device to edit/create
     * \return \e string Eagle command string
     *
     */
	string edit_device(string device_name);

	string call_script(string script_name);

	string change_layer(unsigned int layer_number);

	string change_layer(string layer_name);

	string draw_wire(float x0, float y0, float x1, float y1);

	string draw_wire(float width, float x0, float y0, float x1, float y1);

	string draw_wire(string signal_name, float x0, float y0, float x1, float y1);

	string draw_wire(string signal_name, float width, float x0, float y0, float x1, float y1);

	string draw_circle(float width, float x, float y, float radius);

	string draw_circle(float x, float y, float radius);

	string connect(ConnectType connect_type, string gate_name, string pin_name, string pad_name);

	string connect(ConnectType connect_type, string pin_name, string pad_name);

	string connect(string gate_name, string pin_name, vector<string> pad_names, ConnectType connect_type = ALL);

	string connect(string pin_name, vector<string> pad_names, ConnectType connect_type = ALL);

	string add_package(string package_name, string from_library_name, string variant_name);

	string add_package(string package_name, string variant_name);

	/*
		0(zero) value means width/height won't be used
	*/

	string add_pad(string pad_name, float x, float y, bool is_first = false, PadShape shape = DEFAULT, float diameter = 0);

	string add_smdpad(string pad_name, float x, float y, float width = 0, float height = 0, float rotation = 0, unsigned int roundness = 0);

	string add_pin(string pin_name,float x, float y, unsigned int rotation = 0, PinOptions options=PIN_DEFAULT);

    /** \brief Generate command to place given text to current layer
     *
     *	Rotation must be one of 0,90,180,360.
     *
     * \param text \e string
     * \param x \e float
     * \param y \e float
     * \param rotation = 0 \e unsigned int
     * \return \e string Eagle command string
     *
     */
	string add_text(string text,float x, float y, unsigned int rotation = 0);

	string set_prefix(string prefix);

    /** \brief Generate command to set value of a device to be on or off
     *	If value is on user can change the value of the device. If not
     *	user is asked before changing value of the device.
     * \param on_off \e bool
     * \return \e string Eagle command string
     *
     */
	string set_value_onoff(bool on_off);

}

#endif
