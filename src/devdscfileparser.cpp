#include "devdscfileparser.h"

DevDscFileParser::DevDscFileParser(){

	//initalize default values
	def_sym_style = DeviceDescription::SS_DUAL;

	gates_given = false;
	pads_given = false;

	pin_index = 0;

}

DevDscFileParser::~DevDscFileParser()
{
	//dtor
}

bool DevDscFileParser::parse_file(std::string file_name){

	std::ifstream ifs(file_name.c_str());

	if(!ifs.is_open())
		return false;

	//read all lines
	while(ifs.good()){
		std::string line;
		std::getline(ifs,line);
		lines.push_back(line);
	}

	return parse_lines();

}

bool DevDscFileParser::parse_lines() {

		// active command toggle
	Commands active_command = (Commands)0;

	// remove empty or comment lines, TODO: maybe I should keep them, they are not hard to skip
	make_cleaning();

	//start parsing lines
	for(std::list<std::string>::iterator it=lines.begin(); it!=lines.end(); it++){

		//check if command
		Commands cmd;

		if((cmd=is_command(*it))){
			active_command = cmd;

			//If no gates defined yet, define a default one so pins can be added to
			if(cmd == CPINS) {
				if(!gates_given) devdsc.add_gate(devdsc.name,def_sym_style);
			}

		} else {	// it's not a command read data

			if(active_command>0){	// a command must be active (given) to read data

				switch(active_command){

					case CDEVICE:
						if(!read_device_command(*it)) return false;
						break;

					case CPACKAGE:
						if(!read_package_command(*it)) return false;
						break;

					case CGATES:
						if(!read_gates_command(*it)) return false;
						gates_given = true;
						break;

					case CPINS:
						if(!read_pins_command(*it)) return false;
						break;

					default:
						break;

				}

			} else {
				std::cout<<"Need a command first!"<<std::endl;
				return false;
			}
		}
	}

	// create unique names
	devdsc.create_unames();

	return true;

}

DeviceDescription DevDscFileParser::get_devdsc() {

	return devdsc;

}



void DevDscFileParser::print_lines(){

	for(std::list<std::string>::iterator it=lines.begin();it!=lines.end();it++)
		std::cout<<*it<<std::endl;

}

void DevDscFileParser::make_cleaning(){

	static const boost::regex comment_or_empty_line("\\s*#.*|\\s*");

	std::list<std::string>::iterator it = lines.begin();
	while(it != lines.end()){

		if(regex_match(*it,comment_or_empty_line)){
			it = lines.erase(it);
		}else{
			it++;
		}

	}

}

DevDscFileParser::Commands DevDscFileParser::is_command(const std::string& str){

	static const boost::regex syntax_command_device("\\s*>DEVICE\\s*(#.*)?");
	static const boost::regex syntax_command_package("\\s*>PACKAGE\\s*(#.*)?");
	static const boost::regex syntax_command_gates("\\s*>GATES\\s*(#.*)?");
	static const boost::regex syntax_command_pins("\\s*>PINS\\s*(#.*)?");

	if(regex_match(str,syntax_command_device)){

		return CDEVICE;

	} else if(regex_match(str,syntax_command_gates)) {

		return CGATES;

	} else if(regex_match(str,syntax_command_package)) {

		return CPACKAGE;

	} else if(regex_match(str,syntax_command_pins)) {

		return CPINS;

	}else{
		return (Commands)0;	//not a command
	}

}

bool DevDscFileParser::read_device_command(const std::string& line) {

	static const boost::regex e("\\s*([^\\s^#]+)\\s*([^\\s]+)?\\s*(?:#.*)?");
	boost::smatch what;

	if(regex_match(line,what,e)){
		devdsc.name = what[1];

		if(what[2].length()>0){
			if(!read_symbol_style(what[2],def_sym_style)){
				std::cout<<"Unknown symbol style!"<<std::endl;
				return false;
			}
		}

		return true;

	} else {
		return false;
	}

}

bool DevDscFileParser::read_symbol_style(const std::string& str, DeviceDescription::SymbolStyle& ss){

	if(str=="QUAD"){
		ss = DeviceDescription::SS_DUAL;
	} else if(str=="LEFT") {
		ss = DeviceDescription::SS_LEFT;
	} else if(str=="RIGHT") {
		ss = DeviceDescription::SS_RIGHT;
	} else if(str=="DUAL") {
		ss = DeviceDescription::SS_DUAL;
	} else {
		return false;
	}

	return true;

}
bool DevDscFileParser::read_package_command(const std::string& line){

	static const boost::regex e("\\s*([^\\s^#^@]+)(?:@([^\\s^#]+))?\\s*(?:#.*)?");
    boost::smatch what;

	if(regex_match(line,what,e)){
		devdsc.package_name = what[1];

		if(what[2].length()>0){
			devdsc.package_library = what[2];
		}

		return true;

	} else {
		std::cout<<"Can't read package info!"<<std::endl;
		return false;
	}

}

bool DevDscFileParser::read_gates_command(const std::string& line){

	static const boost::regex e("\\s*([^\\s^#]+)(?:\\s+([^\\s^#]+))?\\s*(?:#.*)?");
	boost::smatch what;

	if(regex_match(line,what,e)){

		DeviceDescription::SymbolStyle sstyle = def_sym_style;

		if(what[2].length()>0){		//if symbol type given

			if(!read_symbol_style(what[2],sstyle)){
				std::cout<<"Unknown symbol stlye"<<std::endl;
				return false;
			}

		}

		devdsc.add_gate(what[1],sstyle);

	} else {
		std::cout<<"Can't read gate info!"<<std::endl;
	}

	return true;

}

// TODO: this is a 250 line of mess, plase tidy it up!
bool DevDscFileParser::read_pins_command(const std::string& line) {

	boost::regex e_pin_pad("\\s*([^\\s^#]+)(?:\\s+([^\\s^#]+))?\\s*(?:#.*)?");
	boost::regex e_bus_pad("\\s*([A-Za-z_!/\\d\\-]+)\\[(\\d)\\.\\.(\\d+)\\](?:\\s+([A-Za-z_!/\\d\\-]+(?:,[A-Za-z_!/\\d\\-]+)+))?\\s*(?:#.*)?");
	boost::regex e_pin_gate_pad("\\s*([^\\s^#]+)\\s+([^\\s^#]+)(?:\\s+([^\\s^#]+))?\\s*(?:#.*)?");
	boost::regex e_bus_gate_pad(
								"\\s*([A-Za-z_!/\\d\\-]+)\\[(\\d)\\.\\.(\\d+)\\]"
								"\\s+([A-Za-z_!/\\d\\-]+)"
								"(?:\\s+([A-Za-z_!/\\d\\-]+(?:,[A-Za-z_!/\\d\\-]+)+))?"
								"\\s*(?:#.*)?"
								);
	boost::smatch what;

	if(gates_given) {

		if(regex_match(line,what,e_bus_gate_pad)){

			// 0: full match
			// 1: bus name
			// 2: index start
			// 3: index end
			// 4: gate name
			// 5: pad names

			std::string bus_name = what[1];

			int bus_start,bus_end,bus_size;
			bus_start = std::stoi(what[2]);
			bus_end = std::stoi(what[3]);
			bus_size = bus_end-bus_start+1;

			std::string gate_name = what[4];
			std::string spad_names = what[5];
			std::vector<std::string> pad_names;

			// are we reading the very first pin
			if(pin_index==0){
				if(spad_names.length()>0) pads_given = true;
			}

			if(pads_given){

				if(spad_names.length()>0){

					pad_names = parse_padnames(spad_names);

					if(bus_size>0){

						if(pad_names.size()==bus_size){

							// everything is okay, now add pins!
							for(int i=0; i<bus_size; i++){
								pin_index++;
								devdsc.add_pin(bus_name+std::to_string(bus_start+i),gate_name,pad_names[i]);
							}

						} else {
							std::cout<<"Number of pad names given is not equal to bus size!"<<std::endl;
							return false;
						}

					} else {
						std::cout<<"Bus end index must be greater then start index."<<std::endl;
						return false;
					}

				} else {
					std::cout<<"You have to define pad names!"<<std::endl;
					return false;
				}

			} else {

				if(bus_size>0){

					for(int i=0; i<bus_size; i++){
						pin_index++;
						devdsc.add_pin(bus_name+std::to_string(bus_start+i),gate_name,std::to_string(pin_index));
					}

				} else {
					std::cout<<"Bus end index must be greater then start index."<<std::endl;
					return false;
				}

			}

		} else if(regex_match(line,what,e_pin_gate_pad)){

			// are we reading the very first pin
			if(pin_index==0){
				if(what[3].length()>0) pads_given = true;
			}
			pin_index++;

			if(pads_given){

				if(what[3].length()>0){
					devdsc.add_pin(what[1],what[2],what[3]);
				} else {
					std::cout<<"You have to define pad names!"<<std::endl;
					return false;
				}

			} else {

				std::stringstream strm;
				strm<<pin_index;
				devdsc.add_pin(what[1],what[2],strm.str());

			}


		} else {
			std::cout<<"Can't read pin info!"<<std::endl;
			return false;
		}

	} else {

		if (regex_match(line,what,e_bus_pad)){

			// 0: full match
			// 1: bus name
			// 2: index start
			// 3: index end
			// 4: pad names

			if(pin_index==0){
				if(what[4].length()>0) pads_given = true;
			}

			if(pads_given){

				std::string bus_name = what[1];

				int bus_start,bus_end,bus_size;
				bus_start = std::stoi(what[2]);
				bus_end = std::stoi(what[3]);
				bus_size = bus_end-bus_start+1;

				std::string spad_names = what[4];
				std::vector<std::string> pad_names;

				if(spad_names.length()>0){

					pad_names = parse_padnames(spad_names);

					if (bus_size>0){

						if(pad_names.size()==bus_size){

							// everything is okay, now add pins!
							for(int i=0; i<bus_size; i++){
								pin_index++;
								devdsc.add_pin(bus_name+std::to_string(bus_start+i),devdsc.gate_list[0].name,pad_names[i]);
							}

						} else {
							std::cout<<"Number of pad names given is not equal to bus size!"<<std::endl;
							return false;
						}

					} else {
						std::cout<<"Bus end index must be greater then start index."<<std::endl;
						return false;
					}

				} else {
					std::cout<<"You have to define pad names!"<<std::endl;
					return false;
				}

			} else {

				std::string bus_name = what[1];

				int bus_start,bus_end,bus_size;
				bus_start = std::stoi(what[2]);
				bus_end = std::stoi(what[3]);
				bus_size = bus_end-bus_start+1;

				if(bus_size>0){

					for(int i=0; i<bus_size; i++){
						pin_index++;
						devdsc.add_pin(bus_name+std::to_string(bus_start+i),devdsc.gate_list[0].name,std::to_string(pin_index));
					}

				} else {
					std::cout<<"Bus end index must be greater then start index."<<std::endl;
					return false;
				}

			}

		} else if(regex_match(line,what,e_pin_pad)){

			if(pin_index==0){
				if(what[2].length()>0) pads_given = true;
			}
			pin_index++;

			if(pads_given) {

				if(what[2].length()>0) {
					devdsc.add_pin(what[1],devdsc.gate_list[0].name,what[2]);
				} else {
					std::cout<<"You have to define pad names!"<<std::endl;
					return false;
				}

			} else {

				devdsc.add_pin(what[1],devdsc.gate_list[0].name,std::to_string(pin_index));

			}

		} else {
			std::cout<<"Can't read pin info!"<<std::endl;
			return false;
		}
	}

	return true;

}

std::vector<std::string> DevDscFileParser::parse_padnames(std::string spadnames) {

	boost::regex e_padname("[A-Za-z_!/\\d\\-]+");
	boost::smatch what;

	std::string::const_iterator start,end;

	start = spadnames.begin();
	end = spadnames.end();

	std::vector<std::string> pad_names;
	while(regex_search(start,end,what,e_padname)){
		pad_names.push_back(what[0]);
		start = what[0].second;
	}

	return pad_names;

}
