#include "devicedescription.h"
#include <limits>

DeviceDescription::DeviceDescription(){
	//ctor
}

bool DeviceDescription::add_gate(std::string name, SymbolStyle style) {

	for(int i=0; i<gate_list.size(); i++){
		if(gate_list.at(i).name == name) return false;
	}

	Gate new_gate;
	new_gate.name = name;
	new_gate.style = style;

	gate_list.push_back(new_gate);

	return true;	//success

}

bool DeviceDescription::add_pin(std::string name, std::string gate_name, std::string pad) {

	for(int i=0; i<gate_list.size(); i++){
		if(gate_list.at(i).name==gate_name){	//find gate by name
			Pin new_pin;
			new_pin.name = name;
			new_pin.pad = pad;
			gate_list.at(i).pin_list.push_back(new_pin);
			return true;	//success
		}else if (i==gate_list.size()-1){
			return false;	//gate name not found
		}
	}

}

void DeviceDescription::set_package(std::string name, std::string library_name) {

	package_name = name;
	package_library = library_name;

}

bool DeviceDescription::read_from_file(std::string file_name) {

	std::ifstream ifs(file_name.c_str());

	if(ifs.is_open()){
		std::string str((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
		return parse_input(str);
	}else{
		return false;
	}
}

bool DeviceDescription::parse_input(std::string str_input) {

	std::stringstream strm;

	bool read_gate,read_pin;
	bool device_read = false;
	bool gates_read = false;
	bool pads_read;
	int pin_index = 0;
	SymbolStyle ss_style;

	strm<<str_input;

	while(strm.good()){
		std::string line;
		std::string c;
		std::string cmdstr;
		std::streampos si;

		//check if this line is comment line
		si = strm.tellg();
		strm>>std::setw(1)>>c;
		if(c == "#"){
			std::getline(strm,line);
			continue;
		} else {	//put that character back
			strm.seekg(si);
		}

		//check if this line is a command
		si = strm.tellg();
		strm>>std::setw(1)>>c;
		if(c==">"){

			strm>>cmdstr;

			//check if command is valid
			if(!((cmdstr=="DEVICE")|
				(cmdstr=="PACKAGE")|
				(cmdstr=="GATES")|
				(cmdstr=="PINS"))){
				std::cout<<"Unknown command!"<<std::endl;
				return false;
			}

			//check if first command is device
			if((!device_read)&&cmdstr!="DEVICE"){
				std::cout<<"First command must be DEVICE!"<<std::endl;
				return false;
			}

			if(cmdstr=="DEVICE"){
				//read_device = true;
				strm.ignore(std::numeric_limits<std::streamsize>::max(),'\n');	//jump to next line

				//TODO:check if name containts any illegal stuff
				strm>>line;
				name = line;

				//check if any symbol type is given, this is hard since it's optional. Skipping white spaces !!
				std::stringstream strm1;
				std::getline(strm,line);
				strm1<<line;
				line.clear();
				strm1>>line;

				if(line.empty()){	//no symbol type given, use default in case no gates given
					ss_style = SS_DUAL;
				}else{
					if(line=="LEFT"){
						ss_style = SS_LEFT;
					}else if(line=="RIGHT"){
						ss_style = SS_RIGHT;
					}else if(line=="DUAL"){
						ss_style = SS_DUAL;
					}else if(line=="QUAD"){
						ss_style = SS_QUAD;
					}else{
						std::cout<<"Cannot recognize symbol style!"<<std::endl;
						return false;
					}
				}

				device_read = true;
				//read_device = false;
				continue;

			}else if(cmdstr=="PACKAGE"){

				strm.ignore(std::numeric_limits<std::streamsize>::max(),'\n');	//jump to next line

				strm>>package_name;
				int spliti = package_name.find_first_of('@');	//check if library name exists and split it from name
				if(spliti>=0){
					package_library = package_name.substr(spliti+1);
					package_name = package_name.substr(0,spliti);
				}

				continue;

			}else if(cmdstr=="GATES"){

				read_gate = true;

				strm.ignore(std::numeric_limits<std::streamsize>::max(),'\n');	//jump to next line

			}else if(cmdstr=="PINS"){

				read_gate = false;
				read_pin = true;

				if(!gates_read){	//if no gate name given, create one with device name
					this->add_gate(name,ss_style);
				}

				strm.ignore(std::numeric_limits<std::streamsize>::max(),'\n');	//jump to next line

			}

		}else{
			strm.seekg(si);
		}

		//try to obtain data
		if(read_gate){

			std::getline(strm,line);
			std::stringstream strm1;
			strm1<<line;

			std::string new_gate_name;
			std::string new_gate_symbol;
			SymbolStyle new_gate_symbol_style;

			strm1>>new_gate_name;
			if(!new_gate_name.empty()){
				strm1>>new_gate_symbol;

				//TODO:check if gate name contains illegal characters
				if(new_gate_symbol=="LEFT"){
					new_gate_symbol_style = SS_LEFT;
				}else if(new_gate_symbol=="RIGHT"){
					new_gate_symbol_style = SS_RIGHT;
				}else if(new_gate_symbol=="DUAL"){
					new_gate_symbol_style = SS_DUAL;
				}else if(new_gate_symbol=="QUAD"){
					new_gate_symbol_style = SS_QUAD;
				}else{
					std::cout<<"Unknown symbol style!"<<std::endl;
					return false;
				}

				this->add_gate(new_gate_name,new_gate_symbol_style);

				gates_read = true;
			}

		}else if(read_pin){

			std::getline(strm,line);
			std::stringstream strm1;
			strm1<<line;

			std::string new_pin_name;
			std::string pin_gate_name;
			std::string pin_pad_name;
			strm1>>new_pin_name;

			if(new_pin_name.empty()) continue;

			if(gates_read && new_pin_name!="NC"){	//do we need gate name
				strm1>>pin_gate_name;
				if(pin_gate_name.empty()){
					std::cout<<"You have to define which gate pins belong!"<<std::endl;
					return false;
				}
			}else{
				pin_gate_name = gate_list.at(0).name;	//use device name as single gate name
			}

			if(pin_index==0){
				strm1>>pin_pad_name;
				if(pin_pad_name.empty()){
					pads_read = false;
					strm1.clear();
					strm1<<pin_index+1;
					strm1>>pin_pad_name;
				}else{
					pads_read = true;
				}
			}else{

				if(pads_read){
					strm1>>pin_pad_name;
					if(pin_pad_name.empty()){
						std::cout<<"Once you did it you have to define all pad names!"<<std::endl;
						return false;
					}
				}else{
					strm1.clear();
					strm1<<pin_index+1;
					strm1>>pin_pad_name;
				}

			}

			this->add_pin(new_pin_name,pin_gate_name,pin_pad_name);

			pin_index++;

		}

	}

	//since whole file read now handle duplicates, create real names
	this->create_unames();

	return true;

}

void DeviceDescription::create_unames() {

	//first create a list of all pins for easy iteration
	std::vector<Pin*> pin_list;

	for(int i=0; i<gate_list.size(); i++){
		for(int j=0; j<gate_list[i].pin_list.size();j++){
			pin_list.push_back(&(gate_list[i].pin_list[j]));
		}
	}

	//now do actual job, find duplicates and give them names
	for(int i=0; i<pin_list.size()-1; i++){

		int ri = 0;

		if(!(pin_list[i]->uname.empty())){
			continue;
		}else{
			pin_list[i]->uname = pin_list[i]->name;
		}

		for(int j=i+1; j<pin_list.size(); j++){

			if(pin_list[j]->name==pin_list[i]->name){

				ri++;

				std::stringstream rnstrm;
				rnstrm<<pin_list[j]->name<<"@"<<ri;

				pin_list[j]->uname = rnstrm.str();

			}

		}

	}

	if(pin_list[pin_list.size()-1]->uname.empty()){
		pin_list[pin_list.size()-1]->uname = pin_list[pin_list.size()-1]->name;
	}

}

std::string DeviceDescription::toString(){

	std::stringstream strm;
	int numpins = 0;

	strm<<"Device name: "<<name<<std::endl;
	strm<<"Number of gates: "<<gate_list.size()<<std::endl;
	strm<<"List of gates:"<<std::endl;
	for(int i=0; i<gate_list.size(); i++){
		strm<<"\t"<<gate_list[i].name;
		strm<<"\t"<<symstyle_toString(gate_list[i].style)<<std::endl;
		numpins += gate_list[i].pin_list.size();
	}
	strm<<std::endl<<"Number of pins: "<<numpins<<std::endl;

	strm<<"List of pins by gate:"<<std::endl;
	for(int i=0; i<gate_list.size(); i++){
		strm<<"\t"<<gate_list[i].name<<std::endl;
		for(int j=0; j<gate_list[i].pin_list.size(); j++){
			strm<<"\t\t"<<gate_list[i].pin_list[j].name<<"\t\t\t-->  "<<gate_list[i].pin_list[j].pad<<std::endl;
		}
	}

	return strm.str();

}

std::string DeviceDescription::symstyle_toString(SymbolStyle ss){

    if(ss==SS_DUAL){
        return "DUAL";
    }else if(ss==SS_LEFT){
        return "LEFT";
    }else if(ss==SS_RIGHT){
        return "RIGHT";
    }else if(ss==SS_QUAD){
        return "QUAD";
    }

}



