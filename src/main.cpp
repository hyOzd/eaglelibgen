#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>

#include "devicedescription.h"
#include "devdscfileparser.h"
#include "eaglelibgen.h"
#include "eaglecomgen.h"

using namespace std;

int main( int argc, char *argv[] ) {

	DeviceDescription dev;
	string file_name;
	//init default options structure
	eaglelibgen::generate_options options;
	options.multi_pin = false;

	if (argc<2) {
        cout<<"Name of the input file (ex: \"thedevice.devdsc\"):"<<endl;
        cin>>file_name;
	}else{
	    file_name = argv[1];

	    //get more options, multi_pin?
	    if(argc>2){

            string arg2 = argv[2];

            if (arg2=="0"){
                options.multi_pin = false;
            }else if(arg2=="1"){
                options.multi_pin = true;
            }

	    }

	}

	cout<<endl<<"Reading... "<<file_name<<endl;

	DevDscFileParser parser;

	if(!parser.parse_file(file_name)){
		cout<<endl<<"Can't read input file or a problem occured! Check file content!"<<endl;
		return 0;
    }

    // parser run successfully
    dev = parser.get_devdsc();

    cout<<endl<<"File read, check results below!"<<endl;

    cout<<dev.toString();

    stringstream newfilename;
    newfilename<<"generate-"<<dev.name<<".scr";

    ofstream ofile(newfilename.str().data());

    if(!ofile.is_open()){
        cout<<"Can't open output file!"<<endl;
        return 0;
    }

    ofile<<eaglelibgen::generate_device(dev,options);

    cout<<endl<<"Generate script is put in "<<newfilename.str()<<" , remember to check contents!"<<endl;

    return 0;
}
