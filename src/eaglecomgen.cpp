#include "eaglecomgen.h"

using namespace std;

string eaglecomgen::open_library(string library_name) {

	stringstream cmdss;

	cmdss<<"OPEN "<<library_name;

	return cmdss.str();

}

string eaglecomgen::copy_package(string package_name,string from_library_name) {

	stringstream cmdss;

	cmdss<<"COPY "<<package_name<<"@"<<from_library_name;

	return cmdss.str();

}

string eaglecomgen::add_symbol(string symbol_name, float x, float y){

	stringstream cmdss;

	cmdss<<"ADD "<<symbol_name<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}

string eaglecomgen::add_symbol(string symbol_name, string gate_name, float x, float y){

	stringstream cmdss;

	cmdss<<"ADD "<<symbol_name<<" "<<gate_name<<" "<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}

string eaglecomgen::edit(string name){

	stringstream cmdss;

	cmdss<<"EDIT "<<name;

	return cmdss.str();

}

string eaglecomgen::edit_device(string device_name){

	return eaglecomgen::edit(device_name+".dev");

}

string eaglecomgen::edit_package(string package_name){

	return eaglecomgen::edit(package_name+".pac");

}

string eaglecomgen::edit_symbol(string symbol_name){

	return eaglecomgen::edit(symbol_name+".sym");

}

string eaglecomgen::call_script(string script_name){

	stringstream cmdss;

	cmdss<<"SCRIPT "<<script_name;

	return cmdss.str();

}

string eaglecomgen::change_layer(string layer_name){

	stringstream cmdss;

	cmdss<<"LAYER "<<layer_name;

	return cmdss.str();

}

string eaglecomgen::change_layer(unsigned int layer_number){

	stringstream cmdss;

	cmdss<<"LAYER "<<layer_number;

	return cmdss.str();

}

string eaglecomgen::draw_wire(string signal_name, float width, float x0, float y0, float x1, float y1){

	stringstream cmdss;

	cmdss<<"WIRE '"<<signal_name<<"' "<<width<<" ("<<x0<<" "<<y0<<") ("<<x1<<" "<<y1<<")";

	return cmdss.str();

}

string eaglecomgen::draw_wire(float x0, float y0, float x1, float y1){

	stringstream cmdss;

	cmdss<<"WIRE ("<<x0<<" "<<y0<<") ("<<x1<<" "<<y1<<")";

	return cmdss.str();

}

string eaglecomgen::draw_wire(string signal_name, float x0, float y0, float x1, float y1){

	stringstream cmdss;

	cmdss<<"WIRE '"<<signal_name<<"' ("<<x0<<" "<<y0<<") ("<<x1<<" "<<y1<<")";

	return cmdss.str();

}

string eaglecomgen::draw_wire(float width, float x0, float y0, float x1, float y1){

	stringstream cmdss;

	cmdss<<"WIRE "<<width<<" ("<<x0<<" "<<y0<<") ("<<x1<<" "<<y1<<")";

	return cmdss.str();

}

string eaglecomgen::draw_circle(float width, float x, float y, float radius){

	stringstream cmdss;

	cmdss<<"CIRCLE "<<width<<" ("<<x<<" "<<y<<") ("<<radius<<" 0)";

	return cmdss.str();

}

string eaglecomgen::draw_circle(float x, float y, float radius){

	stringstream cmdss;

	cmdss<<"CIRCLE ("<<x<<" "<<y<<") ("<<radius<<" 0)";

	return cmdss.str();

}

string eaglecomgen::connect(ConnectType connect_type, string gate_name, string pin_name, string pad_name){

	stringstream cmdss;

	cmdss<<"CONNECT "<<(connect_type==ALL ? "ALL":(connect_type==ANY ? "ANY":""))<<" "<<gate_name<<"."<<pin_name<<" "<<pad_name;

	return cmdss.str();

}

string eaglecomgen::connect(ConnectType connect_type, string pin_name, string pad_name){

	stringstream cmdss;

	cmdss<<"CONNECT "<<(connect_type==ALL ? "ALL":(connect_type==ANY ? "ANY":""))<<" "<<pin_name<<" "<<pad_name;

	return cmdss.str();

}

string eaglecomgen::connect(string gate_name, string pin_name, vector<string> pad_names, ConnectType connect_type) {

	stringstream cmdss;

	cmdss<<"CONNECT "<<(connect_type==ALL ? "ALL":(connect_type==ANY ? "ANY":""))<<" "<<gate_name<<"."<<pin_name<<" '";

	for(unsigned int i=0; i<pad_names.size()-1; i++){

		cmdss<<pad_names[i]<<" ";

	}

	cmdss<<pad_names[pad_names.size()-1]<<"'";

	return cmdss.str();

}

string eaglecomgen::connect(string pin_name, vector<string> pad_names, ConnectType connect_type) {

	stringstream cmdss;

	cmdss<<"CONNECT "<<(connect_type==ALL ? "ALL":(connect_type==ANY ? "ANY":""))<<" "<<pin_name<<" '";

	for(unsigned int i=0; i<pad_names.size()-1; i++){

		cmdss<<pad_names[i]<<" ";

	}

	cmdss<<pad_names[pad_names.size()-1]<<"'";

	return cmdss.str();

}


string eaglecomgen::add_package(string package_name, string from_library_name, string variant_name){

	stringstream cmdss;

	cmdss<<"PACKAGE "<<package_name<<"@"<<from_library_name<<" "<<variant_name;

	return cmdss.str();


}

string eaglecomgen::add_package(string package_name, string variant_name){

	stringstream cmdss;

	cmdss<<"PACKAGE "<<package_name<<" "<<variant_name;

	return cmdss.str();

}

string eaglecomgen::add_pad(string pad_name, float x, float y, bool is_first, PadShape shape, float diameter) {

	stringstream cmdss;

	cmdss<<"PAD ";
	if(diameter>0) cmdss<<diameter;

	if ( shape != DEFAULT ) {

		switch (shape) {
			case SQUARE: 	cmdss<<"square";break;
			case ROUND: 	cmdss<<"round";break;
			case OCTAGON:	cmdss<<"octagon";break;
			case LONG:		cmdss<<"long";break;
			case OFFSET:	cmdss<<"offset";break;
		}

		cmdss<<" ";

	}

	if(is_first) cmdss<<"first ";

	if(!pad_name.empty()) cmdss<<"'"<<pad_name<<"' ";

	cmdss<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}

string eaglecomgen::add_smdpad(string pad_name, float x, float y, float width, float height, float rotation, unsigned int roundness){

	stringstream cmdss;

	cmdss<<"SMD ";

	if(width>0&&height>0) cmdss<<width<<" "<<height<<" ";
	if(roundness>0&&roundness<=100) cmdss<<"-"<<roundness<<" ";
	if(rotation>0&&rotation<=359.9) cmdss<<"R"<<rotation<<" ";
	if(!pad_name.empty()) cmdss<<"'"<<pad_name<<"' ";

	cmdss<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}

string eaglecomgen::add_pin(string pin_name,float x, float y, unsigned int rotation, PinOptions options){

	stringstream cmdss;

	cmdss<<"PIN '"<<pin_name<<"' ";

	if(options&PIN_NC) 		cmdss<<"NC ";
	if(options&PIN_IN) 		cmdss<<"In ";
	if(options&PIN_OUT) 	cmdss<<"Out ";
	if(options&PIN_IO)		cmdss<<"IO ";
	if(options&PIN_OC)		cmdss<<"OC ";
	if(options&PIN_HZ)		cmdss<<"Hiz ";
	if(options&PIN_PAS)		cmdss<<"Pas ";
	if(options&PIN_PWR)		cmdss<<"Pwr ";
	if(options&PIN_SUP)		cmdss<<"Sup ";

	if(options&PIN_DOT)		cmdss<<"Dot ";
	if(options&PIN_CLK)		cmdss<<"Clk ";
	if(options&PIN_DOTCLK)	cmdss<<"DotClk ";

	if(options&PIN_POINT)	cmdss<<"Point ";
	if(options&PIN_SHORT)	cmdss<<"Short ";
	if(options&PIN_MIDDLE)	cmdss<<"Middle ";
	if(options&PIN_LONG)	cmdss<<"Long ";

	switch(rotation){
		case 90:	cmdss<<"R90 ";break;
		case 180:	cmdss<<"R180 ";break;
		case 270:	cmdss<<"R270 ";break;
	}

	cmdss<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}

string eaglecomgen::add_text(string text, float x, float y, unsigned int rotation) {

	stringstream cmdss;

	cmdss<<"TEXT "<<text<<" ";

	switch(rotation){
		case 90:	cmdss<<"R90 ";break;
		case 180:	cmdss<<"R180 ";break;
		case 270:	cmdss<<"R270 ";break;
	}

	cmdss<<"("<<x<<" "<<y<<")";

	return cmdss.str();

}


string eaglecomgen::set_prefix(string prefix){

	stringstream cmdss;

	cmdss<<"PREFIX "<<prefix;

	return cmdss.str();

}

string eaglecomgen::set_value_onoff(bool on_off){

	stringstream cmdss;

	cmdss<<"VALUE "<<(on_off ? "ON":"OFF");

	return cmdss.str();

}

