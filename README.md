eaglelibgen by Hasan Yavuz �ZDERYA heyyo61{at}ozderya.net

	eaglelibgen is a little utility to automate symbol generation for 
	Cadsoft Eagle PCB design software. This utility takes a text file 
	as input (Device Description File-.devdsc what I call) and generates 
	an Eagle script file (.scr). Script contains commands to do following:
		1. Create symbols
		2. Create device
		3. Add symbols(gates) to device
		4. Add package to device (can't create a package)
		5. Connect pins to package
		
	Remember eaglelibgen can't create packages for the moment. You have
	to give a package name (and library name if package is in a different
	library than you are working on).
	
	See etc/ folder for details and example input files.
	
Dependencies:
	boost::regex
